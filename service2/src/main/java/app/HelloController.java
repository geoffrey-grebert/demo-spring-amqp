package app;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    private static final String AMQP_EXCHANGE = "app.message";

    @Autowired
    RabbitTemplate rabbitTemplate;

    @GetMapping("/create/{name}")
    String create(
            @PathVariable String name) {
        rabbitTemplate.convertAndSend(AMQP_EXCHANGE, "create", name);

        return "Hello";
    }

    @GetMapping("/update/{name}")
    String update(
            @PathVariable String name) {
        rabbitTemplate.convertAndSend(AMQP_EXCHANGE, "update", name);

        return "Hello";
    }

    @GetMapping("/delete/{name}")
    String delete(
            @PathVariable String name) {
        rabbitTemplate.convertAndSend(AMQP_EXCHANGE, "delete", name);

        return "Hello";
    }
}
