package app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class AmqpService {

    private static final Logger logger = LoggerFactory.getLogger(AmqpService.class);

    @RabbitListener(queues = "app.message.queue.app1")
    public void onCreateMessage(String message) {
        logger.info("new message created : {}", message);
    }

    @RabbitListener(queues = "app.message.queue.update")
    public void onUpdateMessage(String message) {
        logger.info("new message updated : {}", message);
    }

}
